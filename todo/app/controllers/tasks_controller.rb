class TasksController < ApplicationController
	def new
		@task = Task.new
		render :task_form_modal
	end

	def create
		Task.create(task_params)
		@tasks = Task.all
		render :finish_modal_action
	end

	def show
		@task = Task.find(params[:id])
	end

	def destroy
		@task = Task.find(params[:id])
		@task.destroy
		@tasks = Task.all
		render :finish_modal_action
	end

	def edit
		@task = Task.find(params[:id])
		render :task_form_modal
	end

	def update
		@task = Task.find(params[:id])
		@task.update(task_params)
		@tasks = Task.all
		render :finish_modal_action
	end

	private
	def task_params
		params.require(:task).permit(:title, :note, :completed)
	end
end
